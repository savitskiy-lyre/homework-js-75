const express = require("express");
const Vigenere = require('caesar-salad').Vigenere;
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 8080;

app.get('/encode/:data', (req, res) => {
   res.send(Vigenere.Cipher('data').crypt(req.params.data));
})
app.get('/decode/:data', (req, res) => {
   res.send(Vigenere.Decipher('data').crypt(req.params.data));
})
app.get('/:url', (req, res) => {
   res.send(req.params.url);
})
app.get('/', (req, res) => {
   res.send('Hello');
})
app.post('/encode/', (req, res) => {
   if (!req.body.password) res.status(404).send("Wrong data");
   res.send({message: Vigenere.Cipher(req.body.password).crypt(req.body.message)});
})
app.post('/decode/', (req, res) => {
   if (!req.body.password) res.status(404).send("Wrong data");
   res.send({message: Vigenere.Decipher(req.body.password).crypt(req.body.message)});
})

app.listen(port, () => {
   console.log('We are live ~~!!! ' + port);
})