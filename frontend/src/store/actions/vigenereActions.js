
import {DECODE_URL, ENCODE_URL} from "../../config";
import {axiosApi} from "../../axiosApi";

export const FETCH_ENCODED_MESSAGE_REQUEST = 'FETCH_ENCODED_MESSAGE_REQUEST';
export const FETCH_ENCODED_MESSAGE_SUCCESS = 'FETCH_ENCODED_MESSAGE_SUCCESS';
export const FETCH_ENCODED_MESSAGE_FAILURE = 'FETCH_ENCODED_MESSAGE_FAILURE';
export const FETCH_DECODED_MESSAGE_REQUEST = 'FETCH_DECODED_MESSAGE_REQUEST';
export const FETCH_DECODED_MESSAGE_SUCCESS = 'FETCH_DECODED_MESSAGE_SUCCESS';
export const FETCH_DECODED_MESSAGE_FAILURE = 'FETCH_DECODED_MESSAGE_FAILURE';

export const fetchEncodedMessageRequest = () => ({type: FETCH_ENCODED_MESSAGE_REQUEST});
export const fetchEncodedMessageSuccess = (message) => ({type: FETCH_ENCODED_MESSAGE_SUCCESS, payload: message});
export const fetchEncodedMessageFailure = (err) => ({type: FETCH_ENCODED_MESSAGE_FAILURE, payload: err});

export const fetchEncodedMessage = (message, inpHandler) => {
   return async (dispatch) => {
      try {
         dispatch(fetchEncodedMessageRequest());
         const {data} = await axiosApi.post(ENCODE_URL, message);
         dispatch(fetchEncodedMessageSuccess(data.message));
         inpHandler(data.message);
      } catch (error) {
         dispatch(fetchEncodedMessageFailure(error));
      }
   }
}

export const fetchDecodedMessageRequest = () => ({type: FETCH_DECODED_MESSAGE_REQUEST});
export const fetchDecodedMessageSuccess = () => ({type: FETCH_DECODED_MESSAGE_SUCCESS});
export const fetchDecodedMessageFailure = () => ({type: FETCH_DECODED_MESSAGE_FAILURE});

export const fetchDecodedMessage = (message, inpHandler) => {
   return async (dispatch) => {
      try {
         dispatch(fetchDecodedMessageRequest());
         const {data} = await axiosApi.post(DECODE_URL, message);
         dispatch(fetchDecodedMessageSuccess(data.message));
         inpHandler(data.message);
      } catch (error) {
         dispatch(fetchDecodedMessageFailure(error));
      }
   }
}