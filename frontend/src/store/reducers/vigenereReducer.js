import {
   FETCH_DECODED_MESSAGE_FAILURE,
   FETCH_DECODED_MESSAGE_REQUEST, FETCH_DECODED_MESSAGE_SUCCESS,
   FETCH_ENCODED_MESSAGE_FAILURE,
   FETCH_ENCODED_MESSAGE_REQUEST,
   FETCH_ENCODED_MESSAGE_SUCCESS
} from "../actions/vigenereActions";

const initState = {
   error: null,
   encodedMessage: null,
   encodeLoadingBtn: false,
   decodedMessage: null,
   decodeLoadingBtn: false,
};
export const vigenereReducer = (state = initState, action) => {
   switch (action.type) {
      case FETCH_ENCODED_MESSAGE_REQUEST:
         return {...state, encodeLoadingBtn: true, error: null}
      case FETCH_ENCODED_MESSAGE_SUCCESS:
         return {...state, encodeLoadingBtn: false, encodedMessage: action.payload}
      case FETCH_ENCODED_MESSAGE_FAILURE:
         return {...state, encodeLoadingBtn: false, error: action.payload}
      case FETCH_DECODED_MESSAGE_REQUEST:
         return {...state, decodeLoadingBtn: true, error: null}
      case FETCH_DECODED_MESSAGE_SUCCESS:
         return {...state, decodeLoadingBtn: false, decodedMessage: action.payload}
      case FETCH_DECODED_MESSAGE_FAILURE:
         return {...state, decodeLoadingBtn: false, error: action.payload}
      default:
         return state;
   }
}