import React, {useEffect, useState} from 'react';
import {Grid, IconButton, InputAdornment, Stack, TextField} from "@mui/material";
import {makeStyles} from "@mui/styles"
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import KeyboardIcon from '@mui/icons-material/Keyboard';
import HideSourceIcon from '@mui/icons-material/HideSource';
import LoadingButton from '@mui/lab/LoadingButton';

const useStyles = makeStyles({
   endAdornment: {
      padding: 0
   }
});

const CipherBox = ({encodeHandler, loadingEncodeBtn, decodeHandler, loadingDecodeBtn}) => {
   const classes = useStyles();
   const [encodeInpVal, setEncodeInpVal] = useState('');
   const [encodeInpErr, setEncodeInpErr] = useState(false);
   const [decodeInpVal, setDecodeInpVal] = useState('');
   const [decodeInpErr, setDecodeInpErr] = useState(false);
   const [passwordInpVal, setPasswordInpVal] = useState('');
   const [passwordInpErr, setPasswordInpErr] = useState(false);
   const [showPasswordInp, setShowPasswordInp] = useState(false);

   const onEncodeMessage = () => {
      if (!decodeInpVal) {
         setDecodeInpErr(true);
         setEncodeInpErr(false);
         return
      }
      if (!passwordInpVal) return setPasswordInpErr(true);
      setDecodeInpVal('');
      setEncodeInpVal('');
      setPasswordInpVal('');
      encodeHandler({message: decodeInpVal, password: passwordInpVal}, setEncodeInpVal);
   };
   const onDecodeMessage = () => {
      if (!encodeInpVal) {
         setEncodeInpErr(true);
         setDecodeInpErr(false);
         return
      }
      if (!passwordInpVal) return setPasswordInpErr(true);
      setEncodeInpVal('');
      setDecodeInpVal('');
      setPasswordInpVal('');
      decodeHandler({message: encodeInpVal, password: passwordInpVal}, setDecodeInpVal);
   };

   useEffect(() => {
      if (passwordInpErr && passwordInpVal) setPasswordInpErr(false);
   }, [passwordInpVal, passwordInpErr])
   useEffect(() => {
      if (decodeInpErr && decodeInpVal) setDecodeInpErr(false);
   }, [decodeInpVal, decodeInpErr])
   useEffect(() => {
      if (encodeInpErr && encodeInpVal) setEncodeInpErr(false);
   }, [encodeInpVal, encodeInpErr])

   return (
     <Stack spacing={2} maxWidth={'sm'} mx={"auto"} mt={4}>
        <TextField
          label="Encode"
          multiline
          rows={4}
          variant="outlined"
          error={encodeInpErr}
          value={encodeInpVal}
          onChange={e => setEncodeInpVal(e.target.value)}
        />
        <Grid container justifyContent={"space-evenly"}>
           <LoadingButton
             sx={{borderRadius: '40%'}}
             onClick={onDecodeMessage}
             loading={loadingDecodeBtn}
             variant="standard"
           >
              <ArrowDownwardIcon/>
           </LoadingButton>
           <TextField
             label="Password"
             size={"small"}
             required
             type={showPasswordInp ? "text" : "password"}
             error={passwordInpErr}
             value={passwordInpVal}
             onChange={e => setPasswordInpVal(e.target.value)}
             InputProps={{
                endAdornment: (
                  <InputAdornment position='end'>
                     <IconButton onClick={() => setShowPasswordInp(!showPasswordInp)}>
                        {showPasswordInp ? <KeyboardIcon/> : <HideSourceIcon/>}
                     </IconButton>
                  </InputAdornment>
                ),
                classes: {
                   adornedEnd: classes.endAdornment
                },
             }}
           />
           <LoadingButton
             sx={{borderRadius: '40%'}}
             onClick={onEncodeMessage}
             loading={loadingEncodeBtn}
             variant="standard"
           >
              <ArrowUpwardIcon />
           </LoadingButton>
        </Grid>
        <TextField
          label="Decode"
          multiline
          rows={4}
          variant="outlined"
          error={decodeInpErr}
          value={decodeInpVal}
          onChange={e => setDecodeInpVal(e.target.value)}
        />
     </Stack>
   );
};

export default CipherBox;