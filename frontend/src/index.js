import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {vigenereReducer} from "./store/reducers/vigenereReducer";

const rootReducer = combineReducers({
   vigenere: vigenereReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
))

ReactDOM.render(
  <BrowserRouter>
     <Provider store={store}>
        <App/>
     </Provider>
  </BrowserRouter>
  , document.getElementById('root')
);
