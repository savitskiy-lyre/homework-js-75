import Layout from "./componets/UI/Layout/Layout";
import CipherBox from "./componets/CipherBox/CipherBox";
import {useDispatch, useSelector} from "react-redux";
import {fetchDecodedMessage, fetchEncodedMessage} from "./store/actions/vigenereActions";

const App = () => {
   const loadingEncodeBtn = useSelector(state => state.vigenere.encodeLoadingBtn)
   const loadingDecodeBtn = useSelector(state => state.vigenere.decodeLoadingBtn)
   const dispatch = useDispatch();

   return (
     < Layout>
        <CipherBox
          encodeHandler={(message, inpHandler) => dispatch(fetchEncodedMessage(message, inpHandler))}
          loadingEncodeBtn={loadingEncodeBtn}
          decodeHandler={(message, inpHandler) => dispatch(fetchDecodedMessage(message, inpHandler))}
          loadingDecodeBtn={loadingDecodeBtn}
        />
     < /Layout>
   );
};

export default App;
